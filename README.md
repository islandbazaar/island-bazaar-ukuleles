Island Bazaar is the largest ukulele shop in Southern California. Ukulele concerts, classes, workshops, clubs, and a large selection of ukuleles to choose from. A great place to make music and friends. Shirley Orlando and her staff have been bringing music to Southern California for over 40 years.

Address: 16582 Gothard St, Suite R, Huntington Beach, CA 92647, USA

Phone: 714-843-9350

Website: [https://islandbazaarukes.com](https://islandbazaarukes.com)
